package students.collections;

import java.util.Scanner;

/**
 * CLI для стартового меню
 *
 * @author Яблоков Максим Вадимович {@literal <MVYablokov.SBT@sberbank.ru>}
 */
@SuppressWarnings("squid:S106")
public class StartMenuShell extends CliShell {
    public static void main(String[] args) {
        new StartMenuShell(new Scanner(System.in)).start();
    }

    private StartMenuShell(Scanner in) {
        super(in);
    }

    @Override
    void showOutMenu() {
        System.out.println("Выберите тип коллекции:");
        System.out.println("1. List;");
        System.out.println("2. Set;");
        System.out.println("3. Deque;");
        System.out.println("4. Map.");
    }

    @Override
    boolean handleCommand() {
        switch (in.nextInt()) {
            case 0:
                return true;

            case 1:
                new ListShell(in).start();
                break;

            case 2:
                new SetShell(in).start();
                break;

            case 3:
                new DequeShell(in).start();
                break;

            case 4:
                new MapShell(in).start();
                break;

            default:
                System.out.println("Был произведен некорректный ввод. Повторите.");
        }
        return false;
    }
}
