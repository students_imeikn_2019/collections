package students.collections;

import java.util.Scanner;

/**
 * CLI (command-line interface) реализация интерфейса для работы с сервисами
 *
 * @author Яблоков Максим Вадимович {@literal <MVYablokov.SBT@sberbank.ru>}
 */
public abstract class CliShell implements Shell {
    final Scanner in;

    CliShell(Scanner in) {
        this.in = in;
    }

    public final void start() {
        boolean isNeedReturnToMainMenu = false;
        while (!isNeedReturnToMainMenu) {
            showOutMenu();
            isNeedReturnToMainMenu = handleCommand();
        }
    }

    abstract void showOutMenu();

    abstract boolean handleCommand();
}
