package students.collections;

/**
 * Интерфейс для работы с сервисами
 *
 * @author Яблоков Максим Вадимович {@literal <MVYablokov.SBT@sberbank.ru>}
 */
public interface Shell {
    void start();
}
