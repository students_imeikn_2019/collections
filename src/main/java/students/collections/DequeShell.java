package students.collections;

import java.util.Scanner;

/**
 * CLI для работы с двунаправленной очередью
 *
 * @author Яблоков Максим Вадимович {@literal <MVYablokov.SBT@sberbank.ru>}
 */
@SuppressWarnings("squid:S106")
class DequeShell extends CliShell {
    DequeShell(Scanner in) {
        super(in);
    }

    @Override
    void showOutMenu() {
        // ToDo:
    }

    @Override
    boolean handleCommand() {
        // ToDo:
        return true;
    }
}
