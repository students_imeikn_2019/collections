package students.collections;

import java.util.Scanner;

/**
 * CLI для работы со множеством
 *
 * @author Яблоков Максим Вадимович {@literal <MVYablokov.SBT@sberbank.ru>}
 */
@SuppressWarnings("squid:S106")
class SetShell extends CliShell {
    SetShell(Scanner in) {
        super(in);
    }

    @Override
    void showOutMenu() {
        // ToDo:
    }

    @Override
    boolean handleCommand() {
        // ToDo:
        return true;
    }
}
