package students.collections;

import java.util.*;

/**
 * CLI для работы со списком
 *
 * @author Яблоков Максим Вадимович {@literal <MVYablokov.SBT@sberbank.ru>}
 */
@SuppressWarnings("squid:S106")
class ListShell extends CliShell {
    private final List list = new ArrayList();

    ListShell(Scanner in) {
        super(in);
    }

    @Override
    protected void showOutMenu() {
        System.out.println("Выберите действие над списком:");
        System.out.println("1. showItems;");
        System.out.println("2. isEmpty;");
        System.out.println("3. contains;");
        System.out.println("4. add;");
        System.out.println("...");
    }

    @Override
    protected boolean handleCommand() {
        switch (in.nextInt()) {
            case 0:
                return true;

            case 1:
                System.out.println(list);
                break;

            case 2:
                System.out.println(list.isEmpty());
                break;

            case 3:
                checkForContains();
                break;

            case 4:
                addItem();
                break;

            // ToDo:

            default:
                System.out.println("Был произведен некорректный ввод. Повторите.");
        }
        return false;
    }

    private void checkForContains() {
        System.out.print("Введите объект для поиска: ");
        String item = in.next();
        System.out.println(list.contains(item) ? "Объект содержится в коллекции" : "Объект отсутствует в коллекции");
    }

    private void addItem() {
        System.out.print("Введите объект для ввода: ");
        String item = in.next();
        list.add(item);
        System.out.println();
    }
}
